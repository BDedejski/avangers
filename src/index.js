import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import { AvangerTeam } from './components/AvangerTeam';
import { Battle } from './components/Battle';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import { BattleDetails } from './components/BattleDetails';
import {AllBattles} from './components/AllBattles';
import axios from 'axios';

class App extends React.Component {
    constructor(props){
        let token = 'H4OUJ19yD5kKFvE31mkAhG0SJocdtqBQ';
        axios.defaults.baseURL = 'http://localhost:3000/'
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}` 
        super(props);
       
    }
    componentDidMount(){
       
    }
    render() {
      return (
            <div className="container">
                <BrowserRouter>
                    <Route exact path="/" component={AvangerTeam} />
                    <Route exact path="/battle" component={Battle} />
                    <Route path="/battle/:id" component={BattleDetails} />
                    <Route path="/battles" component={AllBattles}/>
                </BrowserRouter>
            </div>
        );
    }
}

ReactDOM.render(
    <App />
    ,
    document.getElementById('root')

);
