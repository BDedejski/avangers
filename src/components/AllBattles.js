import React from 'react';
import { Link } from 'react-router-dom';
import '../index.css';

function BattleTable(props) {
    return (
        <tr>
            <td>{props.value.battleid}</td>
            <td>{props.value.winners}</td>
            <td>{props.value.losers}</td>
            <td>
                <button className="btn btn-info">
                    <Link to={'/battle/' + props.value.battleid}>Check battle</Link>
                </button>
            </td>
        </tr>
    )
}

export class AllBattles extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            battles: []
        }
    }

    componentDidMount() {
        //TODO GET/battles
        let response = [{ battleid: 11, winners: "avangers", losers: "villians" }, { battleid: 10, winners: "belasica", losers: "barselona" }]
        this.setState({
            battles: response
        })
    }

    renderBattleTable(i) {
        return <BattleTable value={this.state.battles[i]} />;
    }

    render() {
        return (
            <div>
                <div className="container">
                    <h2>Results of all the battles</h2>
                    <hr />
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Battle Id</th>
                                <th scope="col">Winners</th>
                                <th scope="col">Losers</th>
                                <th scope="col">Click to check battle</th>
                            </tr>
                        </thead>
                        {Array.from(this.state.battles, (e, i) => {
                            return (
                                <tbody key={i}>
                                    {this.renderBattleTable(i)}
                                </tbody>
                            )
                        })}
                    </table>
                </div>
            </div>
        )
    }
}