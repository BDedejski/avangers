import React from 'react';
import '../index.css';
import 'bootstrap/dist/css/bootstrap.css';
import { Link } from 'react-router-dom';
import axios from 'axios';

function Avanger(props) {
    return (
        <div className="card">
            <div className="card-body">
                <h5 className="card-title">{props.value.name}</h5>
                <p className="card-text">Health: {props.value.hp}</p>
                <p className="card-text">Attack: {props.value.atq}</p>
                <p className="card-text">Defense: {props.value.def}</p>
                <button onClick={props.onClick} className="btn btn-info">Choose {props.value.name}</button>
            </div>
        </div>
    )
}

function StarWar(props) {
    if (props.value == 0) {
        //I want to pass the props.others like a properties to the class Battle 
        localStorage.setItem('avangers', JSON.stringify(props.other));

        return <div><button className="btn btn-warning">
            <Link to="/battle">Start war</Link>
        </button>
            {/* Properties to be passed that way but not to render the Battle component */}
            {/* <Battle value={props.other}/> */}
        </div>
    }
    else {
        return <div>
            <h2>Choose {props.value} more <span className="avangerText">Avangers</span> to join the battle versus <span className="villianText">Villians</span> </h2>
        </div>
    }
}

export class AvangerTeam extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            avangers: [],
            selectedAvangers: [],
            left: 3,
        }
    }

    async componentDidMount() {
        const avangers = await axios.get('/avengers');
        this.setState ({
            avangers: avangers.data
        })
    }

    handleAvangerClick(i) {
        if (this.state.left == 0) {
            this.setState({
                selectedAvangers: [],
                left: 3
            });
        }
        else {
            //Make the list of selectedAvangers
            const selectedAvanger = this.state.avangers[i];
            var selectedAvangers = this.state.selectedAvangers.slice();
            selectedAvangers.push(selectedAvanger);
            //Delete the avangers that have been selected from the all avangers list
            var avangers = this.state.avangers;
            var index = avangers.indexOf(selectedAvanger);
            if (index > -1) {
                avangers.splice(index, 1);
            }
            this.setState({
                avangers: avangers,
                selectedAvangers: this.state.left < 1 ? [] : selectedAvangers,
                left: this.state.left > 0 ? this.state.left - 1 : this.state.left = 0,
            });
        }
    }

    renderAvanger(i) {
        return <Avanger value={this.state.avangers[i]}
            onClick={() => this.handleAvangerClick(i)} />;
    }

    async resetAvangers() {
        const avangers = await axios.get('/avengers');
        this.setState ({
            avangers: avangers.data,
            selectedAvangers: [],
            left: 3
        })
    }

    render() {
        return (
            <div>
                <div className="container">
                    <StarWar value={this.state.left} other={this.state.selectedAvangers} />
                    <div className="row">
                        <div className="col-11">
                            <h4>You Selected:
                                {Array.from(this.state.selectedAvangers, (e, i) => {
                                return (
                                    <span key={i}>
                                        <i> {e.name}, </i>
                                    </span>
                                )
                            })}
                            </h4>
                        </div>
                        <div className="col-2">
                            <button className="btn btn-success" onClick={() => this.resetAvangers()}> Reset</button>
                        </div>
                    </div>
                    <div className="row">
                        {Array.from(this.state.avangers, (e, i) => {
                            return (
                                <span key={i}
                                    className="col-md-4 col-sm-1 spanica">
                                    {this.renderAvanger(i)}
                                </span>
                            )
                        })}
                    </div>
                </div>
            </div>
        );
    }
}