import React from 'react';
import '../index.css';
import { Link } from 'react-router-dom';

export class BattleDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alive: [],
            dead: []
        };
        const battleId = props.match.params.id;
        console.log(battleId)
    }

    componentDidMount() {
        // const battleId = props.match.params.id;
        //TODO GET REQUEST HERE GET/battle/{id}
        let alive = [{ id: 10, name: "joker", health: 60, attack: 15, defense: 36 }]
        let dead = [{ id: 10, name: "batman", health: 10, attack: 15, defense: 36 }, { id: 10, name: "batman", health: 10, attack: 15, defense: 36 }]
        this.setState({
            alive: alive,
            dead: dead
        })
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-4 offset-4">
                            <h2 className="winnercolor">Winners are Avengers</h2>
                        </div>
                        <div className="col-4">
                            <button className="btn btn-warning">
                            <Link to='/battles'>View all battles</Link>
                            </button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="container">
                                <h2 className="alivecolor">Alive</h2>
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Character Name</th>
                                            <th>Character Health left</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {Array.from(this.state.alive, (e, i) => {
                                            return (
                                                <tr key={i}>
                                                    <td>{e.name}</td>
                                                    <td>{e.health}</td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="container">
                                <h2 className="villianText">Dead</h2>
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Character Name</th>
                                            <th>Character Health left</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {Array.from(this.state.dead, (e, i) => {
                                            return (
                                                <tr key={i}>
                                                    <td>{e.name}</td>
                                                    <td>{e.health}</td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default BattleDetails;