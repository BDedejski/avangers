import React from 'react';
import '@fortawesome/fontawesome-free/css/all.min.css';
import { Link } from 'react-router-dom';
import '../index.css'
import axios from 'axios';

function Avanger(props) {
    return (
        <div className="col-md-4 col-sm-1">
            <p className="avangerText">Avanger</p>
            <hr />
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">{props.value.name}</h5>
                    <p className="card-text"><i className="fas fa-heart"></i> {props.value.hp}</p>
                    <p className="card-text"><i className="fab fa-superpowers"></i> {props.value.atq}</p>
                    <p className="card-text"><i className="fas fa-shield-alt"></i>{props.value.def}</p>
                </div>
            </div>
        </div>
    )
}

function Villain(props) {
    return (
        <div className="col-md-4 col-sm-1">
            <p className="villianText">Villain</p>
            <hr />
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">{props.value.name}</h5>
                    <p className="card-text"><i className="fas fa-heart"></i> {props.value.hp}</p>
                    <p className="card-text"><i className="fab fa-superpowers"></i> {props.value.atq}</p>
                    <p className="card-text"><i className="fas fa-shield-alt"></i>{props.value.def}</p>
                </div>
            </div>
        </div>
    )
}

function BeginBattle(props) {
    //make the post with props post is the state so we need to make it with the state
    //TODO POST REQUEST HERE
    var response = 12412421;
    return <div><button className=" battlebutton btn btn-lg btn-danger">
        <Link to={'/battle/' + response}>Start Battle</Link>
    </button>
    </div>
}

export class Battle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            avangers: [],
            villains: [],
            survided: [],
        };
    }
    
    async componentDidMount() {
        const avangers = localStorage.getItem("avangers");
        const villains = await axios.get('/villains');
        //Randomly selecting 3 villains
        const shuffled = villains.data.sort(() => 0.5 - Math.random());
        let selected = shuffled.slice(0, 3);

        this.setState({
            avangers: JSON.parse(avangers),
            villains: selected
        })
    }

    renderAvanger(i) {
        return <Avanger value={this.state.avangers[i]} />;
    }

    renderVillain(i) {
        return <Villain value={this.state.villains[i]} />
    }

    render() {
        if (this.state.avangers.length > 0) {
            return (
                <div>
                    <h4>Get prepared for battle </h4>
                    <div className="container">
                        <div className="row">
                            {Array.from(this.state.avangers, (e, i) => {
                                return (
                                    <span key={i}>
                                        {this.renderAvanger(i)}
                                    </span>
                                )
                            })}
                        </div>
                        <div className="row">
                            <div className="col-2 offset-4">
                                <BeginBattle value={this.state} />
                            </div>
                        </div>
                        <div className="row">
                            {Array.from(this.state.villains, (e, i) => {
                                return (
                                    <span key={i}>
                                        {this.renderVillain(i)}
                                    </span>
                                )
                            })}
                        </div>
                    </div>
                </div>
            )
        }
        return <div></div>
    }
}
export default Battle;